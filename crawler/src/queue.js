const amqp = require('amqplib/callback_api')
const amznQueue = 'amazon'

const queueUrl = 'amqp://queue'

function initiateConnnection(onMessage) {
  amqp.connect(queueUrl, function (error0, connection) {
    if (error0) {
      console.error('[AMQP] errro: ', error0.message)
      throw error0
    }

    connection.on('error', function (err) {
      if (err.message !== 'Connection closing') {
        console.error('[AMQP] conn error', err.message)
      }
    })

    connection.on('close', function () {
      console.error('[AMQP] closing')
      return
    })

    console.log('[AMQP] connected⚡')

    connection.createChannel(function (error1, channel) {
      if (error1) {
        throw error1
      }

      channel.assertQueue(amznQueue, {
        durable: true,
      })
      channel.prefetch(1)
      console.log(`[AMQP] Waiting for messages in ${amznQueue} queue`)

      channel.consume(amznQueue, (msg) => onMessage(msg, channel), {
        noAck: false,
      })
    })
  })
}

module.exports = {
  initiateConnnection,
}
