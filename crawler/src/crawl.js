const puppeteer = require('puppeteer-extra')
const StealthPlugin = require('puppeteer-extra-plugin-stealth')

puppeteer.use(StealthPlugin())

async function getBrowser() {
  const width = 4019
  const height = 1900
  const browser = await puppeteer.launch({
    args: [
      `--window-size=${width},${height}`,
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-infobars',
      '--disable-dev-shm-usage',
      '--disable-features=VizDisplayCompositor',
      '--window-position=0,0',
      '--ignore-certifcate-errors',
      '--ignore-certifcate-errors-spki-list',
      '--user-agent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/81.0.4044.113 Chrome/81.0.4044.113 Safari/537.36"',
    ],
  })
  return browser
}

async function fetchTitle(queueMsg, channel) {
  const asin = queueMsg.content.toString()
  const url = `https://amazon.de/dp/${asin}`

  console.log(new Date())
  console.log(`Crawling ${url}`)

  const browser = await getBrowser()

  let title

  try {
    const page = await browser.newPage()
    await page.goto(url, { waitUntil: 'domcontentloaded' })

    await page.waitForSelector('body')

    title = await page.evaluate(() => {
      try {
        return document.body.querySelector('#productTitle').innerText
      } catch (error) {
        console.log(error.toString())
      }
    })

    if (!title) {
      throw 'This item does not exist'
    }

    console.log('product Title:', title)
  } catch (error) {
    console.error('[fetchTitle] error:', error.toString())
  } finally {
    channel.ack(queueMsg)
    await browser.close()
  }

  return title
}

module.exports = {
  fetchTitle,
}
