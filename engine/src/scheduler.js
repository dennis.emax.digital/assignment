const cron = require('node-cron')

let scheduledTask

async function startSchedule(task, params, cronTab) {
  try {
    if (!cronTab) throw 'Invalid CRON expression'

    scheduledTask = cron.schedule(cronTab, async () => {
      await task(params)
    })

    // console.log(scheduledTask.stop())

    return true
  } catch (error) {
    console.error(error)
  }
}

async function stopSchedule() {
  if (scheduledTask) {
    scheduledTask.stop()
    return true
  }
}

module.exports = {
  startSchedule,
  stopSchedule,
}
