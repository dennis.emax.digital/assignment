const amqp = require('amqplib/callback_api')
const amznQueue = 'amazon'

const queueUrl = 'amqp://queue'
let channel

function initiateConnnection() {
  amqp.connect(queueUrl, function (error0, connection) {
    if (error0) {
      console.error('[AMQP] errro: ', error0.message);
      throw error0;
    }

    connection.on('error', function (err) {
      if (err.message !== 'Connection closing') {
        console.error('[AMQP] conn error', err.message);
      }
    })

    connection.on('close', function () {
      console.error('[AMQP] closing');
      return;
    });

    console.log('[AMQP] connected ⚡')

    connection.createChannel(function (error1, ch) {
      if (error1) {
        throw error1
      }

      console.log('[AMQP] Sending messages through %s', amznQueue)

      channel = ch
      channel.assertQueue(amznQueue, {
        durable: true,
      })
    })
  })
}

function crawl(message) {
  if (channel) {
    return channel.sendToQueue(amznQueue, Buffer.from(message), {
      persistent: true,
    })
  } else {
    console.error('Channel not ready')
  }
}

function closeConn() {
  if (connection) connection.close()
}

initiateConnnection()

module.exports = {
  crawl,
}
