const express = require('express')
const queue = require('./queue')
const scheduler = require('./scheduler')

const app = express()
app.use(express.json())

app.get('/', function (req, res) {
  res.send('emax.digital coding challenge')
})

app.get('/get-title', async (req, res) => {
  try {
    const asin = req.query.asin

    if (!asin) {
      const errorMessage = 'Invalid asin'
      throw errorMessage
    }

    const qRes = await queue.crawl(asin)

    if (qRes) {
      const msg = `${asin} successfully sent to the queue...⛵⭐☄`
      console.log(msg)
      res.json({ status: 'Queued', message: msg })
    } else {
      const error = `Error processing item`
      console.error(error)
      res.status(500).json({ status: 'Error', message: error })
    }
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: 'Error', message: error })
  }
})

app.get('/get-title-scheduled', async (req, res) => {
  try {
    const asin = req.query.asin
    const cronTab = req.query.crontab

    if (!asin || !cronTab) {
      const errorMessage = 'Invalid asin or crontab'
      throw errorMessage
    }

    const schedule = await scheduler.startSchedule(queue.crawl, asin, cronTab)

    if (schedule) {
      const msg = `${asin} successfully scheduled ${cronTab} ⏰`
      console.log(msg)
      res.json({ status: 'Scheduled', message: msg })
    } else {
      const error = `Error scheduling ${asin}`
      console.error(error)
      res.status(500).json({ status: 'Error', message: error })
    }
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: 'Error', message: error })
  }
})

app.get('/stop-title-schedule', async (req, res) => {
  try {
    const scheduleStopped = await scheduler.stopSchedule()

    if (scheduleStopped) {
      const msg = `Successfully stopped schedule for title fetching`
      console.log(msg)
      res.json({ status: 'ScheduleStopepd', message: msg })
    } else {
      const error = `Schedule is not running`
      console.error(error)
      res.status(500).json({ status: 'Error', message: error })
    }
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: 'Error', message: error })
  }
})

app.listen(3030)
